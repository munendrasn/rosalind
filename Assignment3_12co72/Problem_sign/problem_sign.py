fd1=open("rosalind_sign.txt",'r')
string=fd1.read()
fd1.close()

str1=string[:-1]
from itertools import permutations, product

def merge_product(product):
      
      result = []
      numbers, signs = product
      for i, number in enumerate(numbers):
             sign = signs[i]
             number = int(sign + str(number)+' ')
             result.append(number)
             
      return result

def result(n):
      numbers = list(permutations(range(1, n + 1)))
      signs = list(product('-+', repeat=n))
      results = list(product(numbers, signs))
      results = map(merge_product, results)
      
      return results

results = result(int(str1))
str2= str(len(results))
str2+='\n'

fd=open("output.txt",'w')
fd.writelines(str2)
for r in results:
   fd.writelines(' '.join(map(str, r))+'\n')
fd.close()

