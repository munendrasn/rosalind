fd1=open("rosalind_splc.txt",'r')
string=fd1.readlines()
fd1.close()

string.remove(string[0])#removing fasta string

i=0

str1=''
while i<=len(string)-1:#obtaing original dna seq
    if string[i][:1]=='>':
        break
    else :
        str1+=str(string[i][:-1])
    i+=1
      
list1=string[i:]
i=1
res=[]
while i<=len(list1)-1:
    res.append(list1[i][:-1])#sub strings
    i+=2




for e in res:#deleting substring
    str1=str1.replace(str(e),'')

prot={
      'UUU':'F','CUU':'L','AUU':'I','GUU':'V','UUC':'F','CUC':'L','AUC':'I','GUC':'V','UUA':'L','CUA':'L','AUA':'I','GUA':'V','UUG':'L','CUG':'L','AUG':'M','GUG':'V',
      'UCU':'S','CCU':'P','ACU':'T','GCU':'A','UCC':'S','CCC':'P','ACC':'T','GCC':'A','UCA':'S','CCA':'P','ACA':'T','GCA':'A','UCG':'S','CCG':'P','ACG':'T','GCG': 'A',
      'UAU':'Y','CAU':'H','AAU':'N','GAU':'D','UAC':'Y','CAC':'H','AAC':'N','GAC':'D','UAA':'', 'CAA':'Q','AAA':'K','GAA':'E','UAG':'', 'CAG':'Q','AAG':'K','GAG':'E',
      'UGU':'C','CGU':'R','AGU':'S','GGU':'G','UGC':'C','CGC':'R','AGC':'S','GGC':'G','UGA':'', 'CGA':'R','AGA':'R','GGA':'G','UGG':'W','CGG':'R','AGG':'R','GGG':'G'
     }

rna=str1.replace('T','U')

i=0
str2=''
if rna[:3]!='AUG' and (rna[-3:]=='UAA' or rna[-3:]=='UGA' or rna[-3:]=='UAG'):
        print 'protein is invalid '
        exit
    
while i<=len(rna)-2:#exculding the last character
        if i+3==len(rna):
            str2+=prot[rna[i:]]
        else:
            str2+=prot[rna[i:i+3]]
        i+=3
print str2
fd=open("output.txt",'w')
fd.write(str2)
fd.close()

