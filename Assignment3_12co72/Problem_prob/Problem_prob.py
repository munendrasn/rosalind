fd1=open("rosalind_prob.txt",'r')
string=fd1.readlines()
fd1.close()


str1=string[0][:-1]
string.remove(string[0])
seq=string[0].split()


from math import log

def problem(seq, gc):
      for a in gc:
          p_gc = float(a) / 2
          p_at = (1 - float(a)) / 2
          pmf = {"G": p_gc, "C": p_gc, "A": p_at, "T": p_at}
          yield log(reduce(lambda x, y: x * y, map(lambda k: pmf.get(k), seq)), 10)

def format(result):
    
    fd.write( ' '.join("{:0.3f}".format(r) for r in result))
    
fd=open("output.txt",'w')
format(problem(str1, seq))


fd.close()

