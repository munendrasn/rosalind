fd1=open('rosalind_revp.txt','r')
string=fd1.readlines()
fd1.close()

string.remove(string[0])

seq=''
for i in string:
    seq+=i[:-1]

def revc(sub):
    new=''
    sub1=sub[::-1]
    
    
    for i in sub1:
        if i=='A':
            new+='T'
        elif i=='G':
            new+='C'
        elif i=='C':
            new+='G'
        elif i=='T':
            new+='A'
        else:
            pass
    
    return new
            
    

def revp(s):
    palindromes = []
    for k in range(12, 3, -1):
        for i in range(len(s) - k + 1):
            substr = s[i:i+k]
            if substr == revc(substr):
              palindromes.append((i, k))
    return palindromes

fd=open('output.txt','w')
for palindrome in revp(seq):
    
    fd.writelines( str(palindrome[0] + 1 )+' '+str( palindrome[1])+'\n')
fd.close()    
