fd1=open("rosalind_perm.txt",'r')
string=fd1.read()
fd1.close()

num=int(string)

perms=[]
import itertools
def perm(n):
      perms = list(itertools.permutations(range(1,n+1)))#r-length tuples, all possible orderings, no repeated elements
      return str(len(perms)) + '\n' + \
             '\n'.join([' '.join(map(str, p)) for p in perms])


str2= perm(num)

fd=open("output.txt",'w')
fd.write(str2)
fd.close()
