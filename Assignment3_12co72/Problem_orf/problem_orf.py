fd1=open("rosalind_orf.txt",'r')
string=fd1.readlines()
fd1.close()

string.remove(string[0])
str1=''
for i in string:
    str1+=i[:-1]
rna1=str1.replace('T','U')


global prot
prot={
      'UUU':'F','CUU':'L','AUU':'I','GUU':'V','UUC':'F','CUC':'L','AUC':'I','GUC':'V','UUA':'L','CUA':'L','AUA':'I','GUA':'V','UUG':'L','CUG':'L','AUG':'M','GUG':'V',
      'UCU':'S','CCU':'P','ACU':'T','GCU':'A','UCC':'S','CCC':'P','ACC':'T','GCC':'A','UCA':'S','CCA':'P','ACA':'T','GCA':'A','UCG':'S','CCG':'P','ACG':'T','GCG': 'A',
      'UAU':'Y','CAU':'H','AAU':'N','GAU':'D','UAC':'Y','CAC':'H','AAC':'N','GAC':'D','UAA':'Stop', 'CAA':'Q','AAA':'K','GAA':'E','UAG':'Stop', 'CAG':'Q','AAG':'K','GAG':'E',
      'UGU':'C','CGU':'R','AGU':'S','GGU':'G','UGC':'C','CGC':'R','AGC':'S','GGC':'G','UGA':'Stop', 'CGA':'R','AGA':'R','GGA':'G','UGG':'W','CGG':'R','AGG':'R','GGG':'G'
     }



str3=str1[::-1]#string reversal
str2=''
for e in str3:
        if e=='T':
          str2+='A'
        elif e=='G':
          str2+='C'
        elif e=='A':
          str2+='T'
        elif e=='C':
          str2+='G'
        else :
            exit
rna2=str2.replace('T','U')

def translate_codon(codon):
       protein = None
       if len(codon) == 3 and prot.has_key(codon):
            protein = prot[codon]
            return protein


def protein_strings(s):
       results = []
       indices = []
       l = len(s)
       for i in range(l):
           protein = translate_codon(s[i:i+3])
           if protein and protein == 'M':
              indices.append(i)
       for i in indices:
           found_stop = False
           protein_string = ''

           for j in range(i, l, 3):
               protein = translate_codon(s[j:j+3])

               if not protein:
                  break

               if protein == 'Stop':
                  found_stop = True
                  break
               protein_string += protein

           if found_stop:
              results.append(protein_string)

       return results

res1 = protein_strings(rna1)
res2 = protein_strings(rna2)
res=set(res1+res2)
str2=''
for i in res:
    
    str2+=str(i)+'\n'
fd=open("output.txt",'w')
fd.write(str2)
fd.close()

