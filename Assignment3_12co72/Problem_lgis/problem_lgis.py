fd1=open("rosalind_lgis.txt",'r')
string=fd1.read()
fd1.close()

x=string.index('\n')
str1=string[:x]
str2=string[x+1:]
num1=int(str1)

list1=str2.split(' ')
num=list1[-1][:-1]
list1[-1]=num

seq=[]

for num in range(0, num1):
      seq.append( int(list1[num]))
      

def subsequence(d):
            l = []
            for i in range(len(d)):
                   l.append(max([l[j] for j in range(i) if l[j][-1] < d[i]] or [[]], key=len)+ [d[i]])

            return max(l, key=len)

inc = subsequence(seq)
fd=open("output.txt",'w')
for char in inc:
       fd.writelines(str(char) + ' ')
fd.writelines('\n')
dec = subsequence(list(reversed(seq)))
for char in reversed(dec):
   fd.writelines(str(char) + ' ')

fd.close()

