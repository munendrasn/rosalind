fd1=open("rosalind_long.txt",'r')
string=fd1.read()
fd1.close()

i=1
string=string.replace('Rosalind_','')
list1=string.split(">")


seq=[]
while i<=len(list1)-1:
    seq.append(list1[i][5:-1])
    i+=1

i=0
sub_seq=[]
while i<=len(seq)-1:
    sub_seq.append(seq[i].replace('\n',''))
    i+=1


def superstring(seq, res=''):
      if len(seq) == 0:
           return res

      elif len(res) == 0:
           res = seq.pop(0)
           return superstring(seq, res)

      else:
           for i in range(len(seq)):
                  a = seq[i]
                  l = len(a)
                  for p in range(l / 2):
                        q = l - p
                        if res.startswith(a[p:]):
                             seq.pop(i)
                             return superstring(seq, a[:p] + res)

                        if res.endswith(a[:q]):
                            seq.pop(i)
                            return superstring(seq, res + a[q:])
 


str2=superstring(sub_seq)


fd=open("output.txt",'w')
fd.write(str2)
fd.close()
