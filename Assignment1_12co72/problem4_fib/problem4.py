
fd1 = open("rosalind_fib.txt",'r')
string = fd1.read()
fd1.close()

x = string.index(' ')
str1 = string[:x]
str2 = string[x+1:]
num1 =int(str1)
num2 = int(str2)


def fib(num1,num2):
    if num1==1 or num1==0:
       return 1
    fib1=fib(num1-1,num2)+(num2*fib(num1-2,num2))
    return fib1

result=fib(num1-1,num2)

fd = open("output.txt",'w')
fd.write(str(result))
fd.close()
