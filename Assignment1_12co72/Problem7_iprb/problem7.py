import math

fd1 = open("rosalind_iprb.txt",'r')
string = fd1.read()
fd1.close()

x = string.index(' ')
str1 = string[:x]
str2 = string[x+1:]
k = int(str1)

y = str2.index(' ')
str3 = str2[:y]
m = int(str3)
str4 = str2[y+1:]
n = int(str4)

def nCr(n,r):
        f =  math.factorial
        return float(f(n) / f(r) / f(n-r))

total_pop = k+m+n
product = m*n
probability = 0.0
probability = 1 -(nCr(n,2) + (nCr(m,2)/4) + (product/2))/(nCr(total_pop,2))

str4 = str(probability)
x = float(str4[:7])

fd = open("output.txt",'w')
fd.write(str(x))
fd.close()
