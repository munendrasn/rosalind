import sys

fo = open("rosalind_gc.txt","r")
s  = fo.read()
s+ =">"
lines = s.splitlines()

curr_dna_code = 0
gc_ctr = 0
max_gc_perc = 0.000
max_gc_dna_code = ""
curr_dna_len = 0


for new_line in lines:
  if(new_line[0]=='>'):
     strLen=len(new_line)
     if(new_line[strLen-4:strLen]!=">"):
       curr_dna_code=new_line[strLen-4:strLen]

     if(gc_ctr>0):
       curr_gc_perc=float(gc_ctr)/float(curr_dna_len)
       if (curr_gc_perc>max_gc_perc):
           max_gc_perc = curr_gc_perc
           max_gc_dna_code = curr_dna_code

     gc_ctr = 0
     curr_dna_len = 0

  else:
     curr_dna_len+=len(new_line)
     for letter in new_line:
        if(letter == 'G' or letter == 'C'):
           gc_ctr+=1


sys.stdout.write("Rosalind_"+max_gc_dna_code+"\n")
temp = round(max_gc_perc*100.0,6)
print temp
