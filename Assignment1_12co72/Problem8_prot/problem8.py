
fd1 = open("rosalind_prot.txt",'r')
string = fd1.read()
fd1.close()

prot={
    'UUU':'F','CUU':'L','AUU':'I','GUU':'V','UUC':'F','CUC':'L','AUC':'I','GUC':'V','UUA':'L','CUA':'L','AUA':'I','GUA':'V',
    'UUG':'L','CUG':'L','AUG':'M','GUG':'V','UCU':'S','CCU':'P','ACU':'T','GCU':'A','UCC':'S','CCC':'P','ACC':'T','GCC':'A',
    'UCA':'S','CCA':'P','ACA':'T','GCA':'A','UCG':'S','CCG':'P','ACG':'T','GCG': 'A','UAU':'Y','CAU':'H','AAU':'N','GAU':'D',
    'UAC':'Y','CAC':'H','AAC':'N','GAC':'D','UAA':'', 'CAA':'Q','AAA':'K','GAA':'E','UAG':'', 'CAG':'Q','AAG':'K','GAG':'E',
    'UGU':'C','CGU':'R','AGU':'S','GGU':'G','UGC':'C','CGC':'R','AGC':'S','GGC':'G','UGA':'', 'CGA':'R','AGA':'R','GGA':'G',
    'UGG':'W','CGG':'R','AGG':'R','GGG':'G'
     }

i=0
str2=''

if (len(string)-1)%3 !=0: # with a \n at the end of file
    print 'invalid mRNA sequence '
    exit
else:
    if string[:3]!='AUG' and (string[-3:]=='UAA' or string[-3:]=='UGA' or string[-3:]=='UAG'):
        print 'protein is invalid'
        exit

    while i<=len(string)-2: # exculding the last character
        if i+3==len(string):
            str2+=prot[string[i:]]
        else:
            str2+=prot[string[i:i+3]]
        i+=3

fd= open("output.txt",'w')
fd.write(str2)
fd.close()
