
fd1 = open("rosalind_revc.txt",'r')
string = fd1.read()
fd1.close()

str2=''

if len(string)<=4000:
    str_op=string[::-1]  # string reversal
    for e in str_op:
        if e=='T':
          str2+='A'
        elif e=='G':
          str2+='C'
        elif e=='A':
          str2+='T'
        elif e=='C':
          str2+='G'
        else :
            exit
else :
    print 'length exceeds the limit'
    exit

fd = open("output.txt",'w')
fd.write(str2)
fd.close()
