
fd1 = open("rosalind_dna.txt",'r')
string = fd1.read()
fd1.close()

if len(string)<=4000:
    A_count=string.count('A')
    C_count=string.count('C')
    G_count=string.count('G')
    T_count=string.count('T')

else :
    print 'length exceeds the limit'

print A_count,C_count,G_count,T_count

fd = open("output.txt",'w')
fd.write(str(A_count)+" "+str(C_count)+" "+ str(G_count)+" "+str( T_count))
fd.close()
