fd1=open('rosalind_corr.txt','r')
string=fd1.readlines()
fd1.close()

i=1
list1=[]
while i<=len(string)-1:
    list1.append(string[i][:-1])
    i+=2

def rev(seq):
    str2=''
    str_op=seq[::-1]
    for e in str_op:
        if e=='T':
          str2+='A'
        elif e=='G':
          str2+='C'
        elif e=='A':
          str2+='T'
        elif e=='C':
          str2+='G'
        else :
            exit
    return str2

def mis_match(str1,str2):
    i=0
    count=0
    while i<=len(str1)-1:
        if str1[i]!=str2[i]:
           count+=1
        i+=1
    return count

def get_corrections(dna):
    def occur(match,s_dna):
        count=0
        for s in s_dna:
            if s==match or rev(s)==match:
                count+=1
        return count

    correct_seq=[]
    for s in dna:
        if occur(s,dna)>=2:
            correct_seq.append(s)
            correct_seq.append(rev(s))

    correct_seq=list(set(correct_seq))

    err_read=[x for x in dna if x not in correct_seq]
    corrections=[]

    for i in err_read:
        for c in correct_seq:
            if mis_match(i,c)==1:
                corrections.append((i,c))
    return corrections

correct=get_corrections(list1)


fd=open('output.txt','w')
for s in correct:
    fd.writelines(s[0]+'->'+s[1]+'\n')
fd.close()
