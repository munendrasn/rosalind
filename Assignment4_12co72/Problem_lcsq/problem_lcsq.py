fd1=open('rosalind_lcsq.txt','r')
string=fd1.read()
fd1.close()

list1=string.split('>Rosalind_')

list1.remove(list1[0])

seq=list1[0]
sub=list1[1]

seq=seq.replace('\n','')
sub=sub.replace('\n','')

i=0

while i<=len(seq)-1:
     if not(seq[i]=='A' or seq[i]=='G' or seq[i]=='T' or seq[i]=='A') :
          i+=1
     else:
          break
seq=seq[i:]
i=0


while i<=len(sub)-1:
     if not(sub[i]=='A' or sub[i]=='G' or sub[i]=='T' or sub[i]=='A') :
          i+=1
     else:
          break
sub=sub[i:]


def common_string(table, seq, sub, i, j):
    finalString = ""
    while i != 0 and j != 0:
         if table[i][j] == table[i - 1][j]:
             i -= 1
         elif table[i][j] == table[i][j - 1]:
             j -= 1
         else:
             finalString = seq[i - 1] + finalString
             i -= 1
             j -= 1
    return finalString

def lcs(s1, s2):
     table = [[0 for j in range(len(s2) + 1)] for i in range(len(s1) + 1)]
     for i in range(len(s1) + 1):
         for j in range(len(s2) + 1):
             if i == 0 or j == 0:
                table[i][j] = 0
             elif s1[i - 1] == s2[j - 1]:
                table[i][j] = table[i - 1][j - 1] + 1
             else:
                table[i][j] = max(table[i][j - 1], table[i - 1][j])
     return table

table=lcs(seq,sub)

res=(common_string(table, seq, sub, len(seq), len(sub)))

fd=open('output.txt','w')
fd.write(res)
fd.close()
