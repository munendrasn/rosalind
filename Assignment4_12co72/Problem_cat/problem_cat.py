fd1=open("rosalind_cat.txt",'r')
string=fd1.readlines()
fd1.close()


string.remove(string[0])

seq=''
for i in string:
    seq+=i[:-1]


import re

catalan = {}

def c(n):
   if n in catalan:
      pass
   else:
     if n == 0:
        catalan[n] = 1
     elif n == 1:
        catalan[n] = 1
     else:
        result = 0
        for k in range(1, n+1):
            result += c(k-1)*c(n-k)
        catalan[n] = result

   return catalan[n]

cmps = {'A':'U','U':'A','G':'C','C':'G'}

cat_memo = {}

def cat_ser(seq, debug = False):
    if seq in cat_memo:
       pass
    else:
      if len(seq) == 0:
         cat_memo[seq] = 1
      elif seq == 'AU' or seq == 'UA' or seq == 'CG' or seq == 'GC':
         cat_memo[seq] = 1
      else:
         look_for = cmps[seq[0]]
         result = 0
         for i,s in enumerate(seq):
            if i == 0 or i%2 == 0:
               pass
            elif s == look_for:
                 a_count_before = seq.count('A', 1, i)
                 a_count_after = seq.count('A', i + 1)
                 u_count_before = seq.count('U', 1, i)
                 u_count_after = seq.count('U', i + 1)
                 c_count_before = seq.count('C', 1, i)
                 c_count_after = seq.count('C', i + 1)
                 g_count_before = seq.count('G', 1, i)
                 g_count_after = seq.count('G', i + 1)

                 if a_count_before == u_count_before and c_count_before == g_count_before and a_count_after == u_count_after and c_count_after == g_count_after:
                       result += cat_ser(seq[1:i])*cat_ser(seq[i+1:])
            if debug == True:
                 print i
         cat_memo[seq] = result

    return cat_memo[seq]

res= cat_ser(seq)%1000000
str1=str(res)
print res
fd=open('output.txt','w')
fd.write(str1)
fd.close()
