fd1=open('rosalind_kmp.txt','r')
list1=fd1.readlines()
fd1.close()

list1.remove(list1[0])

seq=''
for i in list1:
    seq+=str(i[:-1])

def kmp_preprocess(s):
        j = -1
        b = [j]
        for i, c in enumerate(s):
            while j >= 0 and s[j] != c:
                 j = b[j]
            j += 1
            b.append(j)
        return b[1:]


results = kmp_preprocess(seq)

fd=open('output.txt','w')
fd.writelines( ' '.join(map(str, results)))
fd.close()
#below code taking lot of time for large input
'''
num=len(seq)-1
p=[0]*(num+1)

i=1
k=-1

while i<=(num):
    while k>=0 and seq[k+1]!=seq[i]:
        k=p[k]
    if seq[k+1]==seq[i]:
        k+=1
    p[i]=k
    i+=1

i=1
while i<=len(p)-1:
    p[i]+=1
    i+=1
str2=''
for i in p:
    str2+=str(i)+' '
print str2
'''




