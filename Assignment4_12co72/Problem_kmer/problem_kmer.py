fd1=open('rosalind_kmer.txt','r')
list1=fd1.readlines()
fd1.close()

list1.remove(list1[0])

seq=''
for i in list1:
    seq+=str(i[:-1])

import itertools

def get_kmers(seq,n):
    list1=['A','C','G','T']
    perms=[''.join(i) for i in itertools.product(list1,repeat=n)]
    count=[0]*len(perms)

    for i,c in enumerate(seq):
        if i+n<=len(seq):
            ind=perms.index(seq[i:i+n])
            count[ind]+=1
    return count

kmers=get_kmers(seq,4)

fd=open('output.txt','w')
fd.writelines( ' '.join(map(str, kmers)))
fd.close()





