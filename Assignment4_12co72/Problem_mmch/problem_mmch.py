fd1=open("rosalind_mmch.txt",'r')
string=fd1.readlines()
fd1.close()


string.remove(string[0])
seq=''
for i in string:
    seq+=i[:-1]


A_count=seq.count('A')
G_count=seq.count('G')
C_count=seq.count('C')
U_count=seq.count('U')

import math
num1=math.factorial(max(A_count,U_count))/(math.factorial(abs(A_count-U_count)))
num2=math.factorial(max(G_count,C_count))/(math.factorial(abs(G_count-C_count)))
max_matching=num1*num2
str1=str(max_matching)

fd=open('output.txt','w')
fd.write(str1)
fd.close()
