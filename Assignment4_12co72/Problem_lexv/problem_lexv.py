fd1=open("rosalind_lexv.txt",'r')
string=fd1.read()
fd1.close()

x=string.index('\n')
str1=string[:x]
str2=string[x+1:-1]

num=int(str2)
list1=str1.split()
str3=''

def lexv(list1,n,word="",words=[]):
       if len(word)!=0:
           words.append(word)
       if len(word)<n:
           for s in list1:
               lexv(list1,n,word+s,words)
       return words

fd=open("output.txt",'w')
str1=''
for word in lexv(list1,num):
     str1=str(word)+'\n'
     fd.writelines(str1)
fd.close()

