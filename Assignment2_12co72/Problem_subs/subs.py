import re

fd1 = open("rosalind_subs.txt",'r')
string = fd1.read()
fd1.close()

string = string[:-1]
x = string.index('\n')

seq = string[:x]
subseq=string[x+1:]
str1='(?='+subseq+')'

pos=[m.start() for m in re.finditer(str1, seq)]
new=[]
str2=''

fd=open("output.txt",'w')
for e in pos:
    str2+=str(e+1)+' '
fd.write(str2)
fd.close()
