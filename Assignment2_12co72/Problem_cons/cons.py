import sys

dna = ''
dum = ''
dnar = []
a = []
c = []
g = []
t = []
mx = ''
mc = 0

fo = open("rosalind_cons.txt","r")
string = fo.read()
fo.close()

inp = string.split('>')
inp.remove('')

for i in range(10):
        dn = inp[i][13:len(inp[i])]
	dna = dn.replace('\n','')
	dnar.append(dna)

for i in range(len(dna)):
	a.append(0)
	c.append(0)
	g.append(0)
	t.append(0)
	for j in range(10):
		if dnar[j][i] == 'A':
			a[i] += 1
		elif dnar[j][i] == 'C':
			c[i] += 1
		elif dnar[j][i] == 'G':
			g[i] += 1
		elif dnar[j][i] == 'T':
			t[i] += 1
for i in range(len(dna)):
	mc = 0
	if a[i] > mc:
		mc = a[i]
		mx = 'A'
	if c[i] > mc:
		mc = c[i]
		mx = 'C'
	if g[i] > mc:
		mc = g[i]
		mx = 'G'
	if t[i] > mc:
		mc = t[i]
		mx = 'T'
	sys.stdout.write(mx)
print '\nA:',

for i in range(len(dna)):
	print str(a[i]),
print '\nC:',

for i in range(len(dna)):
	print str(c[i]),
print '\nG:',

for i in range(len(dna)):
	print str(g[i]),
print '\nT:',

for i in range(len(dna)):
	print str(t[i]),
