import urllib,urllib2
import re

fo = open("rosalind_mprt.txt","r")
inpu = fo.read()
fo.close()

inp = inpu.split('\n')
inp.remove('')

for ide in inp:
	ans = ""
	url = 'http://www.uniprot.org/uniprot/'+ide+'.fasta'
	request = urllib2.Request(url)
	response = urllib2.urlopen(request)
	temp = response.read()
	protein = ''.join(temp.split('\n')[1:])
	for i in range(len(protein)-4+1):
		four = protein[i:i+4]
		if(re.match('N[^P][S|T][^P]',four)):
			ans = (ans+str(i+1)+' ')
	if ans:
		print ide
		print ans
