import math

def fac(n):
     if n == 0:
         return 1
     else:
         return n * fac(n-1)

fd1=open('rosalind_lia.txt','r')
inp=fd1.read()
fd1.close()

kin,nin = inp.split( )
k = int(kin)
n = int(nin)

p = 0.0
tin = math.pow(2,float(k))
t = int(tin)

for i in range (n,t+1) :
           p = float(p) + float( float( fac(t)/ (fac(t-i)*fac(i)) ) * math.pow(0.25,i) * math.pow(0.75,t-i))

p+=0.0005

str2 = str(p)
str2 = str2[:5]

fd = open('output.txt','w')
fd.write(str2)
fd.close()
