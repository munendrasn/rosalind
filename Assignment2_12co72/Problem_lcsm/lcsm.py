
fo = open("rosalind_lcsm.txt","r")
inpu = fo.read()
fo.close()
inp = inpu.split('>')
inp.remove('')

dnar = []

for i in range(100):
	dn = inp[i][13:len(inp[i])]
	dna = dn.replace('\n','')
	dnar.append(dna)

def long_substr(data):
    substr = ''
    for i in range(len(data[0])):
      for j in range(len(data[0])-i+1):
         if j > len(substr) and is_substr(data[0][i:i+j], data):
                substr = data[0][i:i+j]
    return substr

def is_substr(find, data):
    if len(data) < 1 and len(find) < 1:
        return False
    for i in range(len(data)):
        if find not in data[i]:
            return False
    return True

str2= long_substr(dnar)

fd = open("output.txt",'w')
fd.write(str2)
fd.close()
