fd1=open("rosalind_mrna.txt",'r')
string=fd1.read()
fd1.close()

dict1={
        'A':['GCU','GCC','GCA','GCG'],'C':['UGU','UGC'],'D':['GAU','GAC'],'E':['GAA','GAG'],'F':['UUU','UUC'],
        'G':['GGU','GGC','GGA','GGG'],'H':['CAU','CAC'],'I':['AUU','AUC','AUA'],'K':['AAA','AAG'],
        'L':['CUU','CUC','CUG','CUA','UUA','UUG'],'M':['AUG'],'N':['AAU','AAC'],'P':['CCU','CCC','CCA','CCG'],
        'Q':['CAA','CAG'],'R':['CGA','CGG','CGC','CGU','AGA','AGG'],'S':['AGU','AGC','UCU','UCC','UCA','UCG'],
        'T':['ACU','ACC','ACA','ACG'],'V':['GUU','GUC','GUA','GUG'],'W':['UGG'],'Y':['UAU','UAC']
}

sum1=1
i=0
new=[]

if string[:1]!='M':
        print 'protein is invalid '
        exit

for e in dict1:
    num=string.count(e)
    num1=(pow(len(dict1[e]),num))
    new.append(num1)

while i<len(new):
      if new[i]!=0:
         sum1=(sum1*int(new[i]))% 1000000
      i+=1

sum1*=3
sum1%=1000000

fd = open("output.txt",'w')
fd.write(str(sum1))
fd.close()
