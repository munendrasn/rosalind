import re

f = open('rosalind_ctbl.txt', 'r')
contents = f.read()
f.close()
T = contents.strip()

def getCharacterTable(tree):
    def populateCharacters(ct, tree):
        nodes = getProcessedCuts(tree)
        for n in nodes:
          ct[n] = 0

    def incrementCharacterTable(ct, cuts):
        nodes = getProcessedCuts(cuts)
        new_ct = ct.copy()
        for n in nodes:
            new_ct[n] = new_ct[n] + 1
        elems = [str(value) for (key, value) in sorted(new_ct.items())]
        return [''.join(elems)]

    def getProcessedCuts(chs):
        return [re.sub('[\(\)]', '',c) for c in chs.split(',')]

    def findCuts(ct, tree):
        cuts = []
        tree = tree[1:-1]
        paren_depth = 0
        for i in range(len(tree)):
            if tree[i] == '(':
               if paren_depth == 0:
                  cut_start = i
               paren_depth += 1

            elif tree[i] == ')':
                paren_depth -= 1
                if paren_depth == 0:
                   cut_end = i
                   cuts += findCuts(ct, tree[cut_start:cut_end+1])
                   cuts += incrementCharacterTable(ct, tree[cut_start:cut_end+1])
        return cuts

    characters = {}
    tree = tree[:-1]
    populateCharacters(characters, tree)
    cuts = findCuts(characters, tree)
    return cuts

ct = getCharacterTable(T)
fd = open("output.txt",'w')
fd.write( '\n'.join(ct))
fd.close()
