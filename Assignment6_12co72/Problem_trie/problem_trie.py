
fd1 = open("rosalind_trie.txt",'r')
dna_strings = fd1.read().strip().split('\n')
fd1.close()

class Node(object):
   i = 0
   def __init__(self, sym = ''):
        Node.i += 1
        self.i = Node.i
        self.sym = sym
        self.children = []

   def insert(self, s):
      if len(s) != 0:
         in_trie = False
         for child in self.children:
            if child.sym == s[0]:
               in_trie = True
               child.insert(s[1:])
               break
         if not in_trie:
            self.children.append(Node(s[0]))
            self.children[-1].insert(s[1:])

   def get_adj_list(self):
       edges = []
       for child in self.children:
          edges.append((self.i, child.i, child.sym))
          edges += child.get_adj_list()
       return edges

def trie(dna_strings):
     trie = Node()
     for s in dna_strings:
        trie.insert(s)
     return trie.get_adj_list()


fd = open("output.txt",'w')
for edge in trie(dna_strings):
       fd.writelines((' '.join(str(i) for i in edge)))
       fd.writelines('\n')
fd.close()
