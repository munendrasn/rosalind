fd1 = open("rosalind_dbru.txt",'r')
seq = [i for i in fd1.read().split('\n') ]
fd1.close()

seq.remove(seq[-1])
k = len(seq[0])

def rev(dna):
    dna=dna[::-1]
    res=''
    for i in dna:
        if i=='A':
            res+='T'
        elif i=='T':
            res+='A'
        elif i=='G':
            res+='C'
        else:
            res+='G'
    return res

for i in seq:
    str1=rev(i)
    if str1 not in seq:
        seq.append(str1)

list.sort(seq)
seq=set(seq)
ans=[]

for i in seq:
    ans.append('('+i[:k-1]+', '+i[1:k]+')')

fd =open("output.txt",'w')
for i in ans:
    fd.writelines(i+'\n')
fd.close()
