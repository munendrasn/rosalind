
fd1 = open("rosalind_conv.txt",'r')
spec1 = [float(i) for i in fd1.readline().split(' ') ]
spec2 = [float(i) for i in fd1.readline().split(' ')]
fd1.close()

res = []
count = {}

for i in spec1:
    for j in spec2:
        res.append(round(i-j,8))

for i in res:
    count[res.count(i)]=i

m = max(count)
ans = abs(count[m])
str1 = str(m)+'\n'+str(ans)

fd = open("output.txt",'w')
fd.write(str1)
fd.close()
