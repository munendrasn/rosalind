fd1=open('rosalind_edit.txt','r')
string=fd1.read()
fd1.close()

list1=string.split('>Rosalind_')

list1.remove(list1[0])

seq=list1[0]
sub=list1[1]

seq=seq.replace('\n','')
sub=sub.replace('\n','')

i=0

while i<=len(seq)-1:
     if(seq[i]=='1' or seq[i]=='2' or seq[i]=='3' or seq[i]=='4'or seq[i]=='5' or seq[i]=='6' or seq[i]=='7' or seq[i]=='8'or seq[i]=='9'or seq[i]=='0') :
          i+=1
     else:
          break
seq=seq[i:]
i=0

while i<=len(sub)-1:
     if (sub[i]=='1' or sub[i]=='2' or sub[i]=='3' or sub[i]=='4'or sub[i]=='5' or sub[i]=='6' or sub[i]=='7' or sub[i]=='8'or sub[i]=='9'or sub[i]=='0'  ) :
          i+=1
     else:
          break
sub=sub[i:]

def editDistance(s,t):
    d = [[0 for j in range(len(s)+1)] for i in range(len(t)+1)]
    
    for j in range(len(d[0])):
        d[0][j] = j
    for i in range(len(d)):
        d[i][0] = i

    for i in range(1,len(d)):
        for j in range(1, len(d[0])):
            if s[j-1] == t[i-1]:
               d[i][j] = d[i-1][j-1]
            else:
               d[i][j] = min([d[i-1][j] + 1, d[i][j-1] + 1, d[i-1][j-1] + 1 ])#deletion,insertion,edit
    return d

ed = editDistance(seq,sub)

edit_distance = ed[len(sub)][len(seq)]
'''
def traceback(ed, s, t):
    s_prime = s[:]
    t_prime = t[:]
    i = len(ed) - 1
    j = len(ed[0]) - 1

    while not(i == 0 and j == 0):
       current_value = ed[i][j]
       if ed[i][j-1] == (current_value - 1):
            j -= 1
            t_prime = t_prime[:i] + '-' + t_prime[i:]

       elif ed[i-1][j] == (current_value - 1):
            i -= 1
            s_prime = s_prime[:j] + '-' + s_prime[j:]

       else:
           j -= 1
           i -= 1
    return s_prime, t_prime

s_prime, t_prime = traceback(ed, seq, sub)
'''
fd= open('output.txt', 'w')
fd.write(str(edit_distance))
fd.close()

