fd1=open('rosalind_scsp.txt','r')
string=fd1.read()
fd1.close()

list1=string.split('\n')
dna1=str(list1[0])
dna2=str(list1[1])

def problem(s, t):
   table = lcs(s, t)
   z=(common_string(table, s, t, len(s), len(t)))
   u = ""
   i = 0
   j = 0
   for c in z:
     if i <=len(s)-1:
        while s[i]!= c:
           u+= s[i]
           i+= 1
        i+= 1

     if j <=len(t)-1:
        while t[j] != c:
          u += t[j]
          j += 1
        j += 1
     u += c
   if i < len(s):
      u += s[i:]
   if j < len(t):
      u += t[j:]
   return u

def common_string(table, seq, sub, i, j):
    finalString = ""
    while i != 0 and j != 0:
         if table[i][j] == table[i - 1][j]:
             i -= 1
         elif table[i][j] == table[i][j - 1]:
             j -= 1
         else:
             finalString = seq[i - 1] + finalString
             i -= 1
             j -= 1
    return finalString
def lcs(s1, s2):
     table = [[0 for j in range(len(s2) + 1)] for i in range(len(s1) + 1)]
     for i in range(len(s1) + 1):
         for j in range(len(s2) + 1):
             if i == 0 or j == 0:
                table[i][j] = 0
             elif s1[i - 1] == s2[j - 1]:
                table[i][j] = table[i - 1][j - 1] + 1
             else:
                table[i][j] = max(table[i][j - 1], table[i - 1][j])
     return table


res= problem(dna1,dna2)
print res
fd=open('output.txt','w')
fd.write(res)
fd.close()
