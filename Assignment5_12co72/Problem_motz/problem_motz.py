fd1 = open('rosalind_motz.txt', 'r')
contents = fd1.readlines()
fd1.close()


contents.remove(contents[0])

seq=''
for i in contents:
    seq+=i[:-1]

motzin_memo = {}

def motzin(n):
    if n in motzin_memo:
       pass
    else:

       if n == 0:
          motzin_memo[n] = 1
       elif n == 1:
          motzin_memo[n] = 1
       else:
          result = motzin(n-1)
          for k in range(2,n+1):
              result += motzin(k-2)*motzin(n-k)
          motzin_memo[n] = result
    return motzin_memo[n]

cmps = {'A':'U','U':'A','G':'C','C':'G'}

motzinRNA_memo = {}

def motzinRNA(s):
     if s in motzinRNA_memo:
         pass
     else:
         if len(s) == 0:
            motzinRNA_memo[s] = 1
   # one possible matching possibility:
         elif s == 'A' or s == 'U' or s == 'C' or s == 'G':
            motzinRNA_memo[s] = 1
# meat of it here:
         else:
            result = motzinRNA(s[1:])
            comp = cmps[s[0]]
            for i,base in enumerate(s[1:]):
# if we have a match, we'll cut it out, and then add on the result of the number of motzin matches from the product of the two substrings:
                if base == comp:
                   abs_cut_end = i+1
                   str1 = s[1:abs_cut_end]
                   str2 = s[abs_cut_end + 1:]
                   result += motzinRNA(str1)*motzinRNA(str2)
            motzinRNA_memo[s] = result
     return motzinRNA_memo[s]
    
result = motzinRNA(seq)
res= result%1000000
str1=str(res)

fd=open('output.txt','w')
fd.write(str1)
fd.close()
