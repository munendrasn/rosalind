fd=open("rosalind_aspc.txt",'r')
string=fd.read()
fd.close()

string=string[:-1]
list1=string.split()
num=int(list1[0])
k=int(list1[1])

import math

res=0
while k<=num:
    res+=(math.factorial(num)/(math.factorial(num-k)*math.factorial(k)))
    k+=1

res= res%1000000

str1=str(res)
fd=open("output.txt",'w')
fd.write(str1)
fd.close()
