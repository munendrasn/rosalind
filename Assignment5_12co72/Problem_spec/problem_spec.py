fd1=open("rosalind_spec.txt",'r')
list1=fd1.readlines()
fd1.close()

for i in list1:
    i=float(i[:-1])

num=len(list1)-1

dict1={'A':71.03711,'C':103.00919,'D':115.02694,'E':129.04259,'F':147.06841,'G':57.02146,'H':137.05891,
       'I':113.08406,'K':128.09496,'L':113.08406,'M':131.04049,'N':114.04293,'P':97.05276,
       'Q':128.05858,'R':156.10111,'S':87.03203,'T':101.04768,'V':99.06841,
       'W':186.07931,'Y':163.06333}


import itertools

def protein_string(s):
    result = ''
    
    inverted_mass = {}
    for k, v in dict1.iteritems():#inverting mass table
         inverted_mass[round(v, 4)] = k
    
    for i in range(1, len(s)):
         a = float(s[i - 1])
         b = float(s[i])
         result += inverted_mass[round(b - a, 4)]
    return result

res=protein_string(list1)
print res
fd=open("output.txt",'w')
fd.write(res)
fd.close()
