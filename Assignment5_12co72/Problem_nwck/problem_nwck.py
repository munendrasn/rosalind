fd1 = open('rosalind_nwck.txt', 'r')
contents = fd1.read().strip()
fd1.close()

contents = contents.split('\n')
trees = contents[::3]
node_lines = contents[1::3]
nodes = [n.split(' ') for n in node_lines]
import re
#Return: A collection of n positive integers, for which the kth integer represents the distance between xk and yk in Tk.
trees_info = zip(trees, nodes)
def getDistance(t):
    def preprocess(t):
#get rid of any other plant names
        t = re.sub('\w','',t)
        removed = ''
        while removed != t:
           removed = t
           t = re.sub('\([^\(]+?\)',',',t, 1)
        return t

    def parseIntervals(intervals):
        points = {}
        points[',+'] = 2
        points[',+)'] = 1
        points[')'] = 1
        points['(,+'] = 1
        points['('] = 1

        score = 0;
        comma_flag = False
        started_flag = False
        rparen_flag = False
        for i in range(len(intervals)):
            if not started_flag:
               if intervals[i] == ',':
                  started_flag = True
                  comma_flag = True
               if intervals[i] == '(':
                  started_flag = True
                  rparen_flag = True
               if intervals[i] == ')':
                  score += points[')']

            else:
                  if comma_flag == True and intervals[i] == ',' and rparen_flag == False:
                            pass
                  elif comma_flag == True and intervals[i] == ')' and rparen_flag == False:
                            comma_flag = False
                            started_flag = False
                            score += points[',+)']
                  elif comma_flag == True and intervals[i] == '(' and rparen_flag == False:
                            comma_flag = False
                            rparen_flag = True
                            score += points[',+']
                  elif rparen_flag == True and intervals[i] == ',' and comma_flag == True:
                            pass
                  elif rparen_flag == True and intervals[i] == ',' and comma_flag == False:
                            comma_flag = True
                  elif rparen_flag == True and intervals[i] == '(' and comma_flag == False:
                            score += points['(']
                  elif rparen_flag == True and intervals[i] == '(' and comma_flag == True:
                            score += points['(,+']
        if started_flag == True and comma_flag == True and rparen_flag == False:
                      score += points[',+']
        elif started_flag == True and comma_flag == False and rparen_flag == True:
                      score += points['(']
        elif started_flag == True and comma_flag == True and rparen_flag == True:
                      score += points['(,+']
        return score;

    n1 = t[1][0]
    n2 = t[1][1]
    newick = t[0]
    intervals = re.split(n1 + '|' + n2, newick)[1]
    intervals = preprocess(intervals)
    distances = parseIntervals(intervals)
    return distances

distances = [getDistance(d) for d in trees_info]
fd=open('output.txt','w')
fd.write( ' '.join([str(d) for d in distances]))
fd.close()
