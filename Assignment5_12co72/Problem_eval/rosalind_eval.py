fd1 = open('rosalind_eval.txt', 'r')
contents = fd1.read().strip()
fd1.close()
contents = contents.split('\n')
n = int(contents[0])
s = contents[1]
A = [float(i) for i in contents[2].split(' ')]

#Return: An array B having the same length as A in which B[i] represents the expected number of times that s will appear as a substring of a random DNA string t of length n, where t is formed with GC-content A[i] (see "Introduction to Random Strings").
B = []

def getExpected(n,s,gc):
    options = n + 1 - len(s)
    expected = 0
    for o in range(options):
        p = 1
        for c in s:
            if c == 'A' or c == 'T':
                 p *= (1-gc)/2
            else:
                 p *= gc/2
        expected += p
    return expected

B = [getExpected(n,s,a) for a in A]


fd = open('output.txt', 'w')
for b in B:
  fd.write(str(b+0.0005)[:5] + ' ')
fd.close()
