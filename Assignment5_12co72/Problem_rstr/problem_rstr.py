fd1 = open('rosalind_rstr.txt', 'r')
contents = fd1.read().strip()
fd1.close()
contents = contents.split('\n')
s = contents[1]
N, GC = [float(c) for c in contents[0].strip().split(' ')]

p = 1.0
for n in s:
  if n == 'A' or n == 'T':
     p *= (1-GC)/2
  else:
     p *= GC/2
# p is the probabililty of it being exactly the same string.
# so (1-p) is the probability of it not being that string.
# (1-p)**N is the probability of it not being that string N times
# therefore, 1 - (1-p)**N is the probability of it being that string at least once.

res=(1 - (1.0-p)**N)

res+=+0.0005
str1=str(res)[:5]

fd= open('output.txt', 'w')
fd.write(str1)
fd.close()
