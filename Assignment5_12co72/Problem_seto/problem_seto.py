fd1=open("rosalind_seto.txt",'r')
U= set(range(1, int(fd1.readline().strip()) + 1))
A = set(int(i) for i in fd1.readline().strip()[1:-1].split(", "))
B = set(int(i) for i in fd1.readline().strip()[1:-1].split(", "))
fd1.close()

def set_op(U,A,B):
    return [A.union(B), A.intersection(B), A - B, B - A, U - A, U - B]

res=[]
for s in set_op(U, A, B):
   res.append(list(s))

for i in res:
    i=str(i)
str1=''
for  i in res:
    str1+= '{'+str(i)+'}'+'\n'

fd=open("output.txt",'w')
fd.write(str1)
fd.close()

fd=open("output.txt",'r')
str2=fd.read()
fd.close()

str2=str2.replace('[','')
str2=str2.replace(']','')
fd=open("output.txt",'w')
fd.write(str2)
fd.close()
